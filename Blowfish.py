import struct


class Blowfish:
    def __init__(self, key):
        self.block_size = 8  # 64 bits
        self.rounds = 16
        self.p = self.init_p()
        self.s = self.init_s()
        self.key = key
        self.expand_key()

    def init_p(self):
        # Initialize the P-array with the hexadecimal digits of pi
        p = [
            0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344,
            0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89,
            0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C,
            0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917,
            0x9216D5D9, 0x8979FB1B
        ]
        for i in range(18):
            p[i] ^= self.key[i % len(self.key)]
        return p

    def init_s(self):
        # Initialize the S-boxes with digits of pi
        s = [
            [
                0xD1310BA6, 0x98DFB5AC, 0x2FFD72DB, 0xD01ADFB7,
                0xB8E1AFED, 0x6A267E96, 0xBA7C9045, 0xF12C7F99,
                0x24A19947, 0xB3916CF7, 0x0801F2E2, 0x858EFC16,
                0x636920D8, 0x71574E69, 0xA458FEA3, 0xF4933D7E,
                0x0D95748F, 0x728EB658, 0x718BCD58, 0x82154AEE,
                0x7B54A41D, 0xC25A59B5, 0x9C30D539, 0x2AF26013,
                0xC5D1B023, 0x286085F0, 0xCA417918, 0xB8DB38EF,
                0x8E79DCB0, 0x603A180E, 0x6C9E0E8B, 0xB01E8A3E,
                0xD71577C1, 0xBD314B27, 0x78AF2FDA, 0x55605C60,
                0xE65525F3, 0xAA55AB94, 0x57489862, 0x63E81440,
                0x55CA396A, 0x2AAB10B6, 0xB4CC5C34, 0x1141E8CE,
                0xA15486AF, 0x7C72E993, 0xB3EE1411, 0x636FBC2A,
                0x2BA9C55D, 0x741831F6 ]]
            def expand_key(self):
        # Expand the key using the Blowfish key schedule
        l = [0] * 2
        r = [0] * 2

        # Split the key into 32-bit words
        key_words = struct.unpack(">" + "L" * 8, self.key)

        # XOR the key with the P-array
        for i in range(18):
            l[i % 2] ^= key_words[i % 8]
            self.encrypt(l, r)
            self.p[i] = l[i % 2]
            self.p[(i + 1) % 18] = r[i % 2]

        # XOR the subkeys with the S-boxes
        for i in range(4):
            for j in range(256):
                l[i % 2] ^= key_words[i * 2 + j % 2]
                r[i % 2] ^= self.p[j]
                self.encrypt(l, r)
                self.s[i][j] = l[i % 2]
                self.s[i][(j + 1) % 256] = r[i % 2]

    def encrypt(self, l, r):
        # Perform the Blowfish encryption algorithm on a block
        for i in range(self.rounds):
            l[i % 2] ^= self.p[i]
            r[i % 2] ^= self.F(l[i % 2])
            l[(i + 1) % 2], r[(i + 1) % 2] = r[i % 2], l[i % 2]
        l[(self.rounds + 1) % 2] ^= self.p[self.rounds]
        r[(self.rounds + 1) % 2] ^= self.p[self.rounds + 1]

    def F(self, x):
        # Apply the Blowfish Feistel function to a 32-bit word
        h = [0] * 4
        h[0] = (x >> 24) & 0xFF
        h[1] = (x >> 16) & 0xFF
        h[2] = (x >> 8) & 0xFF
        h[3] = x & 0xFF
        y = sum(h[i] << ((3 - i) * 8) for i in range(4))
        z = self.s[0][h[0]] + self.s[1][h[1]] ^ self.s[2][h[2]] + self.s[3][h[3]]
        return ((z ^ y) & 0xFFFFFFFF)

    def encrypt_block(self, block):
        # Encrypt a single 64-bit block of data
        l = [0] * 2
        r = [0] * 2
        l[0], r[0] = struct.unpack(">LL", block)
        self.encrypt(l, r)
        return struct.pack(">LL", l[self.rounds % 2], r[self.rounds % 2])

    def decrypt_block(self, block):
        # Decrypt a single 64-bit block of data
        l = [0] * 2
        r = [0] * 2
        l[0], r[0] = struct.unpack(">LL", block)
        self.decrypt(l, r)
        return struct.pack(">LL", l[self.rounds % 2], r[self.rounds % 2])


    def decrypt(self, l, r):
        # Perform the Blowfish decryption algorithm on a block
        for i in range(self.rounds):
            l[i % 2] ^= self.p[self.rounds + 1 - i]
            r[i % 2] ^= self.F(l[i % 2])
            l[(i + 1) % 2], r[(i + 1) % 2] = r[i % 2], l[i % 2]
        l[(self.rounds + 1) % 2] ^= self.p[0]
        r[(self.rounds + 1) % 2] ^= self.p[1]

    def encrypt_data(self, data):
        # Encrypt a byte string using CBC mode
        # The IV is set to a fixed value of 0
        iv = b"\x00" * self.block_size
        encrypted_data = b""
        prev_block = iv
        for i in range(0, len(data), self.block_size):
            block = data[i:i+self.block_size]
            block = self.xor_blocks(block, prev_block)
            prev_block = self.encrypt_block(block)
            encrypted_data += prev_block
        return encrypted_data

    def decrypt_data(self, data):
        # Decrypt a byte string using CBC mode
        # The IV is set to a fixed value of 0
        iv = b"\x00" * self.block_size
        decrypted_data = b""
        prev_block = iv
        for i in range(0, len(data), self.block_size):
            block = data[i:i+self.block_size]
            decrypted_block = self.decrypt_block(block)
            decrypted_block = self.xor_blocks(decrypted_block, prev_block)
            prev_block = block
            decrypted_data += decrypted_block
        return decrypted_data

    def xor_blocks(self, block1, block2):
        # XOR two byte strings of equal length
        return bytes([b1 ^ b2 for b1, b2 in zip(block1, block2)])
