import hashlib
import random


class Point:
    def __init__(self, x, y, a, b):
        self.x = x
        self.y = y
        self.a = a
        self.b = b

        # Check if the point is at infinity
        if self.x is None and self.y is None:
            return

        # Check if the point satisfies the elliptic curve equation
        if pow(y, 2, b) != pow(x, 3, b) + a * x + b:
            raise ValueError("Point ({}, {}) is not on the curve".format(x, y))

    def __repr__(self):
        if self.x is None:
            return "Point(infinity)"
        else:
            return "Point({}, {})".format(self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.a == other.a and self.b == other.b

    def __add__(self, other):
        if self.a != other.a or self.b != other.b:
            raise ValueError("Points ({}, {}) and ({}, {}) are not on the same curve".format(self.x, self.y, other.x, other.y))

        # Check if one of the points is at infinity
        if self.x is None:
            return other
        elif other.x is None:
            return self

        # Check if the points are equal
        if self == other:
            s = (3 * pow(self.x, 2, self.b) + self.a) * pow(2 * self.y, -1, self.b)
        else:
            s = (other.y - self.y) * pow(other.x - self.x, -1, self.b)

        x = pow(s, 2, self.b) - self.x - other.x
        y = -(s * (x - self.x) + self.y)
        return Point(x, y, self.a, self.b)

    def __mul__(self, scalar):
        # Use double-and-add algorithm to compute scalar multiplication
        res = Point(None, None, self.a, self.b)
        current = self

        while scalar:
            if scalar & 1:
                res += current
            current += current
            scalar >>= 1

        return res


class ECDSA:
    def __init__(self, curve):
        self.curve = curve
        self.n = curve.order
        self.G = curve.generator

  def sign(self, private_key, message):
        # Compute the hash of the message
        h = int.from_bytes(hashlib.sha256(message.encode()).digest(), 'big')

        while True:
            # Generate a random integer k between 1 and n-1
            k = random.randint(1, self.n - 1)

            # Compute the point R = k * G
            R = k * self.G

            # Compute r = x(R) mod n
            r = R.x % self.n
            if r == 0:
                continue

            # Compute s = k^(-1) * (h + d * r) mod n
            k_inv = pow(k, -1, self.n)
            s = (k_inv * (h + private_key * r)) % self.n
            if s == 0:
                continue

            # Return the signature (r, s)
            return (r, s)

    def verify(self, public_key, signature, message):
        # Unpack the signature
        r, s = signature

        # Compute the hash of the message
        h = int.from_bytes(hashlib.sha256(message.encode()).digest(), 'big')

        # Compute w = s^(-1) mod n
        w = pow(s, -1, self.n)

        # Compute u1 = hw mod n and u2 = rw mod n
        u1 = (h * w) % self.n
        u2 = (r * w) % self.n

        # Compute the point R = u1 * G + u2 * public_key
        R = u1 * self.G + u2 * public_key

        # Verify if r = x(R) mod n
        if r == R.x % self.n:
            return True
        else:
            return False