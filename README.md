
# Encryption Algorithms

This repository contains examples of various encryption algorithms implemented in Python.

## Algorithms

The following encryption algorithms are currently included in this repository:

* RSA Encryption: A public-key encryption algorithm based on the difficulty of factoring large integers.
* Advanced Encryption Standard (AES): A symmetric-key encryption algorithm that can encrypt (encipher) and decrypt (decipher) information.
* Blowfish: A symmetric-key block cipher encryption algorithm designed by Bruce Schneier.

Each algorithm implementation includes examples of how to use the algorithm to encrypt and decrypt data.